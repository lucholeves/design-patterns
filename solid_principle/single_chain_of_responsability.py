# SCRP
# A class should only have one reason to change
# Separation of concerns - different classes handling different, independent tasks/problems

class Journal:
    def __init__(self):
        self.entries = []
        self.count = 0

    def add_entry(self, text):
        self.count += 1
        self.entries.append(f'{self.count}: {text}')

    def remove_entry(self, pos):
        del self.entries[pos]

    def __str__(self):
        return '\n'.join(self.entries)

    # Anti pattern
    # Persistence methods should be in a separate class cause they are not related with a journal
    # def save(self, filename):
    #     with open(filename, 'w') as file:
    #         file.write(str(self))
    #
    # def load(self, filename):
    #     pass
    #
    # def load_from_web(self, uri):
    #     pass


class PersistenceManager:

    @staticmethod
    def save_to_file(journal, filename):
        with open(filename, 'w') as file:
            file.write(str(journal))


j = Journal()
j.add_entry('I cried today.')
j.add_entry('I ate a bug.')
print(f'Journal entries:\n{j}')

filename = 'journal.txt'
PersistenceManager.save_to_file(j, filename)

with open(filename) as file:
    print(file.read())
