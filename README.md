# Design patterns

## Creational patterns
- Deal with the creation (construction) of objects
- Explicit (constructor) vs. implicit (DI, reflection, etc.)
- Wholesale (single statement) vs. piecewise (step-by-step)

### Builder
When piecewise object is complicated, provide an API for doing it succinctly

### Factory
Any entity that can take care of object creation

### Prototype
A partially or fully initialized object that you copy (clone) and make use of

### Singleton
A component which is instantiated only once

## Structural patterns
- Concerned with the structure (e.g. class members)
- Many patterns are wrappers that mimic the underlying class's interface
- Stress the importance of good API design

### Adapter
A construct which adapts an existing interface X to conform to the required interface Y

### Bridge
A mechanism that decouples an interface (hierarchy) from an implementation (hierarchy)

### Composite
A mechanism for treating individual (scalar) objects and compositions of objects in a uniform manner

### Decorator
Facilitates the addition of behaviours to individual objects without inheriting from them

### Facade
Provides a simple, easy to understand/user interface over a large and sophisticated body of code

### Flyweight
A space optimization technique that lets us use less memory by storing externally the data associated with similar objects

### Proxy
A class that functions as an interface to a particular resource. That resource may be remote, expensive to construct, or may require logging or some other added functionality

## Behavioral patterns
- They are all different; no central theme

### Chain of responsibility
A chain of components who all get a chance to process a command or a query, optionally having default processing implementation, and an ability to terminate the processing chain

### Command
An object which represents an instruction to perform a particular action. Contains all the information necessary for the action to be taken

### Interpreter
A component that processes structured text data. Does so by turning it into separate lexical tokens (lexing) and the interpreting sequences of said tokens (parsing)

### Iterator
An object that facilitates the traversal of a data structure

### Mediator
A component that facilitates communication between other components without them necessarily being aware of each other or having direct (reference) access to each other

### Memento
A token/handle representing the system state. Lets us roll back to the state when the token was generated. May or may not directly expose state information

### Observer
An observer is an object that wishes to be informed about events happening in the system. The entity generating the event is an observable

### State
A pattern in which the object's behavior is determined by its state. An object transitions from one state to another (something needs to trigger a transition).
A formalized construct which manages state and transitions is called state machine

### Strategy
Enables the exact behavior of a system to be selected at run-time

### Template method
Allows us to define the 'skeleton' of the algorithm, with concrete implementations defined in subclasses

### Visitor
A component (visitor) that knows how to traverse a data structure composed of (possibly related) types
