class Node:
    def __init__(self, value, left=None, right=None):
        self.right = right
        self.left = left
        self.value = value

        self.parent = None

        if left:
            self.left.parent = self
        if right:
            self.right.parent = self

    def traverse_preorder(self):
        yield self.value
        if self.left:
            yield from self.left.traverse_preorder()
        if self.right:
            yield from self.right.traverse_preorder()


if __name__ == '__main__':
    root = Node(
        1, Node(2, Node(4), Node(5)), Node(3)
    )
    for node in root.traverse_preorder():
        print(node)
