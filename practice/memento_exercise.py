class Token:
    def __init__(self, value=0):
        self.value = value


class Memento(list):
    pass


class TokenMachine:
    def __init__(self):
        self.tokens = []

    def add_token_value(self, value):
        return self.add_token(Token(value))

    def add_token(self, token):
        self.tokens.append(token)
        return Memento([token.value for token in self.tokens])

    def revert(self, memento):
        self.tokens = [Token(value) for value in memento]

    def __str__(self):
        return str([token.value for token in self.tokens])


if __name__ == '__main__':
    tm = TokenMachine()

    token1 = Token(1)
    token2 = Token(2)
    m1 = tm.add_token(token1)
    m2 = tm.add_token(token2)
    print(tm)

    tm.revert(m1)
    print(tm)
